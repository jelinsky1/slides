# Awesome Title
Your Name, @twitter

<small>Goettingen State and University Library</small>

---

![an image](img/some-image.png)

you can insert Images…

[…and Hyperlinks](https://de.dariah.eu/)

--

![from wikicommons](https://upload.wikimedia.org/wikipedia/commons/8/8b/Campsite_at_Mystic_Beach%2C_Vancouver_Island%2C_Canada.jpg)


--

## Headline 2

- Lists
  - sublists
  - yes
- another point

--

### Headline 3

Some awesome text here.

--

<!-- .slide: data-background-color="lightblue" -->

## Background Color Makes Life Good

--

<!-- .slide: data-background-image="img/some-image.png" -->
![a single transparent pixel](img/pixel.png) <!-- .element height="100%" width="100%" -->

### Background Image

---

What about a table? You can easily create one with the help of [this tool](https://www.tablesgenerator.com/markdown_tables).

| Head 1 | Head 2 | Head 3 |
|----------|--------|--------|
| centered | left | right |
| content | align | align |
