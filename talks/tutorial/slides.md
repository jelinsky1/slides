# Presentations with Markdown
Julian Jelinsky, @jelinsky1

<small>Göttingen State and University Library</small>

---

Disclaimer: This is not a feature presentation.
A demo is available at [https://revealjs.com](https://revealjs.com).

|||

…but since we are preparing a presenation as a website, we can include other websites…

|||

<!-- .slide: data-background-iframe="https://revealjs.com" -->

|||

Cool!

|||

Do not expect all features to be available with markdown.

|||

So what is this markdown thing?

|||

<!-- .slide: data-background-iframe="https://en.m.wikipedia.org/wiki/Markdown" -->

|||

To keep it short: Markdown is an easy to use markup language.

Do not get scared by the term „markup language“. In fact it is text, written in the most simple editor. You can structure your text by headlines, you can have **bold** and ***italics*** and so on.

|||

When you write a few sentence, you will get familiar to this.

|||

Save your markdown files with the file name extension `.md`.

|||

Markdown is powerful, easy and very limited. You can not let things <span class="inline fragment grow">grow</span> or <span class="inline fragment shrink">shrink</span> with it.

|||

Features like this need some additions. We will see it later.

|||

<span class="fragment fade-in">
  <span class="fragment highlight-red">
  <span class="fragment highlight-blue">
    <span class="fragment fade-out">
      This is what <em>revealjs</em> is doing. 
    </span>
  </span>
  </span>
</span>

---

# How to get there?

|||

We will use our computing centers installation of GitLab to get this with the help of GitLab Pages.
And we there is a prepared repo, you just have to <em>copy</em>. In git terms, this means to fork the repo.

|||

## Login
log in to GitLab at [gitlab.gwdg.de](https://gitlab.gwdg.de)

|||

<!-- .slide: data-background-iframe="https://gitlab.gwdg.de" -->

|||

Argl! This does not work. GWDG do not want to see the GitLab inside a presentation! They set the HTTP header `X-Frame-Options: deny`. Damn it!

|||

## Fork the repo

![fork](img/gitlab-repo-fork.png)

|||

<!-- .slide: data-transition="fade-out" -->

Now you have your private instance! What is in there?

```text
└── lib
└── talks
    └── generic-markdown
        ├── custom.css
        ├── img
        │   └── some-image.png
        ├── index.html
        └── slides.md
```

|||

<!-- .slide: data-transition="fade-in" -->

Now you have your private instance! What is in there?

```text
└── lib # nothing we want to discuss here
└── talks # directory where to put in your talks
    └── generic-markdown # sample presentation
        ├── custom.css # for any new or changed styles
        ├── img # directory to put in your images
        │   └── some-image.png
        ├── index.html # website enty point
        └── slides.md # markdown file
```

|||

## Edit

To edit the files, we can use the GitLab web interface. Look out for the "WebIDE" button.
Select the file `talks / tutorial / slides.md`.

|||

### slides.md

This is the main file and the place where to write your presentation.

|||

Simply start by adjusting the headline.

```md
# Headline
```

|||

You can add text in the markdown syntax as you like.
In addition to standard markdown, there is a code for the slide separators.

```md
--- # for horizontal separators
||| # for vertical separators
```

both must be surrounded by empty new lines (regex: `^\r?\n---\r?\n$`). 

|||

### index.html

3 things are not part of the markdown document:
+ Title (to be read in browser title)
+ Description (for you and the search engine)
+ Your name (for others and the search engine)

(Add meta data here as you like, there is [a common vocabulary](https://schema.org/docs/documents.html).)

|||

## PDF export

Sure. Simply browse to the URL that ends with `index.html?print-pdf`. Replace it in the adress bar.

Use Chromium for this. :-|

Use the browser \[CTRL + P] to “Print to PDF”.

---

# More to read
You can read about this in [a corresponding article](https://lab.sub.uni-goettingen.de/markdown-reveal-pages.html) at our blog. And <span class="inline fragment grow">you</span> can publish articles there as well!

|||

# Contact me

![contact me](img/contact-me.jpg)
